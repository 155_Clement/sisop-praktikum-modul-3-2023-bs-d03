# No 1
## lossless.c
### 1. Includes dan define
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// For toupper()
#include <ctype.h>
// For child process
#include <sys/types.h>
#include <sys/wait.h>

#define CHAR_AM 26
```
`CHAR_AM` adalah jumlah huruf.

### 2. Frequency struct
```
// Store character frequency
typedef struct charFreq
{
    char ch;
    int freq;
} cF;
```
- Struct untuk menyimpan informasi frekuensi sebuah karakter.

### 3. Huffman tree
Kode huffman tree berdasarkan kode dari geeksforgeeks. Digunakan struct untuk node tree dan informasi min heap.
```
//  Worst case tree depth = N-1 (N = amount of characters encoded)
#define MAX_TREE_HT 25 

// A Huffman tree node
struct MinHeapNode
{
    //Both character and frequency
    cF data;

    // Left and right child of this node
    struct MinHeapNode *left, *right;
};

// A Min Heap:  Collection of
// min-heap (or Huffman tree) nodes
struct MinHeap
{

    // Current size of min heap
    unsigned size;

    // capacity of min heap
    unsigned capacity;

    // Array of minheap node pointers
    struct MinHeapNode **array;
};
```
Huffman tree akan dibuat dengan :
- Membuat min heap
- Selama isi min heap > 1  :
    - Mengambil 2 anggota terkecil dalam min heap
    - Membuat node internal dengan nilai frekuesi adalah total kedua anggota tersebut.
    - Memasang node internal sebagai parent kedua anggota tersebut (Membangun huffman tree sambil membaca min heap).
    - Memasukkan node internal kedalam min heap.
- Anggota terakhir setelah proses ini adalah root node dari huffman tree.

```
// A utility function allocate a new
// min heap node with given character
// and frequency of the character
struct MinHeapNode *newNode(cF input)
{
    struct MinHeapNode *temp = (struct MinHeapNode *)malloc(
        sizeof(struct MinHeapNode));

    temp->left = temp->right = NULL;
    temp->data.ch = input.ch;
    temp->data.freq = input.freq;

    return temp;
}

// A utility function to create
// a min heap of given capacity
struct MinHeap *createMinHeap(unsigned capacity)

{

    struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

    // current size is 0
    minHeap->size = 0;

    minHeap->capacity = capacity;

    minHeap->array = (struct MinHeapNode **)malloc(
        minHeap->capacity * sizeof(struct MinHeapNode *));
    return minHeap;
}
```
Berikut adalah fungsi untuk inisialisasi node baru dan untuk inisialisasi min heap (jika diberikan kapasitas awal).
Dalam program, min heap berupa array berisi node-node yang belum terhubung, sedangkan nantinya huffman tree akan berupa binary tree antar node.

```
// A utility function to
// swap two min heap nodes
void swapMinHeapNode(struct MinHeapNode **a,
                     struct MinHeapNode **b)

{

    struct MinHeapNode *t = *a;
    *a = *b;
    *b = t;
}

// The standard minHeapify function.
void minHeapify(struct MinHeap *minHeap, int idx)

{

    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < minHeap->size && minHeap->array[left]->data.freq < minHeap->array[smallest]->data.freq)
        smallest = left;

    if (right < minHeap->size && minHeap->array[right]->data.freq < minHeap->array[smallest]->data.freq)
        smallest = right;

    if (smallest != idx)
    {
        swapMinHeapNode(&minHeap->array[smallest],
                        &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}
```
Berikut adalah fungsi untuk menukar posisi 2 node (`swapMinHeapNode()`) dan untuk mengubah sebuah subtree menjadi min heap (`minHeapify()`).
- `minHeapify()` :
    - Mengambil id root, child kiri, dan child kanan.
    - Menukar posisi root dan isi nodenya jika nilai (frekuensi huruf) salah satu child lebih kecil (menukar dengan yang terkecil).
    - Melakukan `minHeapify()` kepada subtreenya.

```
/ A utility function to check
// if size of heap is 1 or not
int isSizeOne(struct MinHeap *minHeap)
{

    return (minHeap->size == 1);
}

// A standard function to extract
// minimum value node from heap
struct MinHeapNode *extractMin(struct MinHeap *minHeap)

{

    struct MinHeapNode *temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];

    --minHeap->size;
    minHeapify(minHeap, 0);

    return temp;
}
```
Berikut adalah fungsi untuk memeriksa apakah ukuran min heap tinggal 1 (`isSizeOne()`) dan mengambil node dengan nilai minimal (root) dalam min heap (`extractMin`).
```
// A utility function to insert
// a new node to Min Heap
void insertMinHeap(struct MinHeap *minHeap,
                   struct MinHeapNode *minHeapNode)

{

    ++minHeap->size;
    int i = minHeap->size - 1;

    while (i && minHeapNode->data.freq < minHeap->array[(i - 1) / 2]->data.freq)
    {

        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }

    minHeap->array[i] = minHeapNode;
}

// A standard function to build min heap
void buildMinHeap(struct MinHeap *minHeap)

{

    int n = minHeap->size - 1;
    int i;

    for (i = (n - 1) / 2; i >= 0; --i)
        minHeapify(minHeap, i);
}
```
Berikut adalah fungsi untuk menambah isi baru ke dalam min heap (`insertMinHeap()`) sambil menjaga kondisi min heap serta fungsi untuk membuat menjadi heap suatu array (`buildMinHeap()`).

```
// A utility function to print an array of size n
void printArr(int arr[], int n, char one_res[50])
{
    int i;
    char itoc;
    strcpy(one_res, "");
    for (i = 0; i <= n; ++i){
        
            itoc = arr[i]+'0';

            //If end of string
            if(i >= n){
                one_res[i] = '\0';
                break;
            }
            if(itoc == '0' || itoc == '1'){
                one_res[i] = itoc;
            }
            
    }

    //printf("|| %s\n", one_res);
    //    printf("%d", arr[i]);
}
```
Berikut adalah fungsi untuk memindahkan isi suatu array ke dalam array lain. Fungsi ini digunakan untuk memindahkan huffman code ke array output.
```
// Utility function to check if this node is leaf
int isLeaf(struct MinHeapNode *root)

{

    return !(root->left) && !(root->right);
}

// Creates a min heap of capacity
// equal to size and inserts all character of
// data[] in min heap. Initially size of
// min heap is equal to capacity
struct MinHeap *createAndBuildMinHeap(cF input[], int size)

{

    struct MinHeap *minHeap = createMinHeap(size);

    for (int i = 0; i < size; ++i)
        minHeap->array[i] = newNode(input[i]);

    minHeap->size = size;
    buildMinHeap(minHeap);

    return minHeap;
}
```
Berikut adalah fungsi untuk memeriksa apakah node ini leaf (`isLeaf()`) dan fungsi untuk membuat, lalu mengisi suatu array dalam struct MinHeap, dan menggunakan `buildMinHeap()` menjadikannya min heap.
```
// The main function that builds Huffman tree
struct MinHeapNode *buildHuffmanTree(cF input[], int size)

{
    struct MinHeapNode *left, *right, *top;

    // Step 1: Create a min heap of capacity
    // equal to size.  Initially, there are
    // modes equal to size.
    struct MinHeap *minHeap = createAndBuildMinHeap(input, size);

    // Iterate while size of heap doesn't become 1
    while (!isSizeOne(minHeap))
    {

        // Step 2: Extract the two minimum
        // freq items from min heap
        left = extractMin(minHeap);
        right = extractMin(minHeap);

        // Step 3:  Create a new internal
        // node with frequency equal to the
        // sum of the two nodes frequencies.
        // Make the two extracted node as
        // left and right children of this new node.
        // Add this node to the min heap
        // '$' is a special value for internal nodes, not
        // used
        cF input = {'$', left->data.freq + right->data.freq};
        top = newNode(input);

        top->left = left;
        top->right = right;

        insertMinHeap(minHeap, top);
    }

    // Step 4: The remaining node is the
    // root node and the tree is complete.
    return extractMin(minHeap);
}
```
Berikut adalah fungsi utama yang membangun huffman tree.
```
// Prints huffman codes from the root of Huffman Tree.
// It uses arr[] to store codes
void printCodes(struct MinHeapNode *root, int arr[],
                int top, char res [][50])

{

    // Assign 0 to left edge and recur
    if (root->left)
    {

        arr[top] = 0;
        printCodes(root->left, arr, top + 1, res);
    }

    // Assign 1 to right edge and recur
    if (root->right)
    {

        arr[top] = 1;
        printCodes(root->right, arr, top + 1, res);
    }

    // If this is a leaf node, then
    // it contains one of the input
    // characters, print the character
    // and its code from arr[]
    if (isLeaf(root))
    {

        //printf("%c: ", root->data.ch);
        printArr(arr, top, res[root->data.ch-65]);
    }
}
```
Berikut adalah fungsi yang membangun string huffman code dengan menelusuri huffman tree secara rekursif dan mencetak kode jika menemukan suatu leaf.
```
void HuffmanCodes(cF input[], int size, char res [][50])

{
    // Construct Huffman Tree
    struct MinHeapNode *root = buildHuffmanTree(input, size);

    // Print Huffman codes using
    // the Huffman tree built above
    int arr[MAX_TREE_HT], top = 0;

    printCodes(root, arr, top, res);
}
```
Berikut adalah driver code yang membuat huffman tree (`buildHuffmanTree()`) dan menyimpannya ke dalam array hasil (`printCodes()`).
### 4. Main (awal)
```
int main()
{
    int fd1[2];
    int fd2[2];

    pid_t parent;

    pipe(fd1);
    pipe(fd2);

    parent = fork();
```
- Menyiapkan pipe.
- Melakukan fork.
### 5. Parent
```
    if (parent > 0)
    {
        FILE *ori = fopen("file.txt", "r");
        cF freq[CHAR_AM];
        // Stores file length (only alphabets)
        int fileLength = 0;
        // Init
        for (int x = 0; x < CHAR_AM; x++)
        {
            freq[x].ch = (char) 65 + x;
            freq[x].freq = 0;
        }
        char ch;
        // Reads character 1 by 1
        while ((ch = fgetc(ori)) != EOF)
        {
            
            // If lowercase
            if ((ch >= 'a') && (ch <= 'z'))
            {
                fileLength++;
                ch = toupper(ch);
                freq[ch - 'A'].ch = ch;
                freq[ch - 'A'].freq += 1;
            }
            // else if uppercase
            else if ((ch >= 'A') && (ch <= 'Z'))
            {
                fileLength++;
                freq[ch - 'A'].ch = ch;
                freq[ch - 'A'].freq += 1;
            }
            else
            {
                continue;
            }
            // else (if neither)
        }
        
        fclose(ori);
        // Sends result to child
        close(fd1[0]);

        write(fd1[1], freq, sizeof(cF) * CHAR_AM);
        // write(fd1[1], &fileLength, sizeof(int));
        close(fd1[1]);

        //Receives child result
        char codes[MAX_TREE_HT+1][50];
        close(fd2[1]);
        read(fd2[0], codes, sizeof(codes));
        close(fd2[0]);
        //Prints results
        for(int x = 0; x < CHAR_AM; x++){
            printf("%c : ", (x+65));
            for(int y = 0; y < 50; y++){
                if(!codes[x][y]){
                    break;
                }
                printf("%c", codes[x][y]);
            }
            printf("\n");
        }
        printf("Initial size (only alphabets) : %d bits\n", fileLength*8);
        //Calculates encrypted size
        FILE *encrypted = fopen("encrypted.txt", "a");
        int fSize;
        // Move stream (cursor) to end of file, measure size of file by position of stream
        fseek(encrypted, 0L, SEEK_END);
        fSize = ftell(ori);
        printf("Size after encryption (only alphabets) : %d bits\n", fSize);
        fclose(encrypted);
        wait(NULL);


    }
```
- Mengakses `file.txt`.
- Inisialisasi array struct `cF` (`freq`).
- Membaca satu per satu karakter lalu menghitung frekuensi karakter ke dalam `freq[]` + menghitung jumlah karakter huruf.
- Menutup file `file.txt`.
- Mengirim hasil frekuensi ke child process menggunakan pipe 1.
-------------------------
- Jika child process selesai membuat huffman code, menerima hasil melalui pipe 2.
- Mencetak hasil ke terminal.
- Membaca isi file hasil enkripsi `file.txt` (hanya huruf).
- Mencetak ukuran bits file awal (hanya huruf) dan bits file hasil enkripsi.
- Selesai
### 6. Child 
```
    else
    {
        // Receives frequency from  parent
        cF freqCpy[CHAR_AM];
        //2D array to store pairs of characters and huffman codes
        char codes[MAX_TREE_HT+1][50];
        // int fileLength;

        close(fd1[1]);

        read(fd1[0], freqCpy, sizeof(cF) * CHAR_AM);
        // read(fd1[0], &fileLength, sizeof(int));
        // printf("Length : %d\n", fileLength);

        close(fd1[0]);

        // Gets file content
        FILE *ori = fopen("file.txt", "r");
        // Opens encrypted file
        FILE *encrypted = fopen("encrypted.txt", "w");
        int fSize;
        char *buffer;
        // Move stream (cursor) to end of file, measure size of file by position of stream
        fseek(ori, 0L, SEEK_END);
        fSize = ftell(ori);
        // Rewind stream back to beginning
        rewind(ori);
        /*Alocate memory*/
        buffer = calloc(1, fSize + 1);
        // Copy
        fread(buffer, fSize, 1, ori);

        // for(int x = 0; x < 26; x++){
        //     printf("%c || %d\n", freqCpy[x].ch, freqCpy[x].freq);
        // }
        char ch;
        HuffmanCodes(freqCpy, 26, codes);

        //ENCRYPT
        for(int x = 0; x <= fSize; x++){
            ch = *(buffer+x);
            // If lowercase
            if ((ch >= 'a') && (ch <= 'z'))
            {

                ch = toupper(ch);
                fprintf(encrypted, "%s", codes[ch-65]);
            }
            // else if uppercase
            else if ((ch >= 'A') && (ch <= 'Z'))
            {

                fprintf(encrypted, "%s", codes[ch-65]);
            }
            else
            {
                continue;
            }
        }
        //Send huffmancode to parent
        close(fd2[0]);
        write(fd2[1], codes, sizeof(codes));
        close(fd2[1]);
        // Cleanup
        fclose(ori);
        fclose(encrypted);
        free(buffer);
    }
```
- Menerima jumlah frekuensi huruf dari parent process menggunakan pipe 1.
- Membuka `file.txt` dan `encrypted.txt` (hasil enkripsi).
- Membaca isi `file.txt` ke dalam `buffer`.
- Membuat huffman code (`HuffmanCodes()`) menggunakan jumlah frekuensi huruf dan menyimpannya ke dalam `codes[][]`.
- Menulis enkripsi huffman setiap huruf dari `file.txt` ke dalam `encrypted.txt`.
- Mengirim huffman code (`codes[][]`) ke parent menggunakan pipe 2.
- cleanup.
-------------


No 2

Soal 

a. Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

b. Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar.
(Catatan: wajib menerapkan konsep shared memory)

c. Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh:
        
             array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],

maka:
        
             1 2 6 24 120 720 ... ... …

(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)

d. Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak.
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

Penyelesaian :

kalian.c 

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>


// untuk baris dan kolom matriks 1
#define B1 4
#define K1 2
//  untuk baris dan kolom matriks 2
#define B2 2
#define K2 5


int** Matrix(int baris, int kolom, int BatasAtas, int BatasBawah) {
	int** matriks = malloc(baris * sizeof(int*));
	srand(time(NULL));
	for (int n=0; n<baris; n++) {
    	matriks[n] = malloc(kolom * sizeof(int));
    	for (int m=0; m<kolom; m++) {
        	matriks[n][m] = (rand() % (BatasAtas - BatasBawah + 1)) + BatasBawah;
    	}
	}
	return matriks;
}


int main()
{
	int **mat1 = Matrix(B1, K1, 5, 1);
	int **mat2 = Matrix(B2, K2, 4, 1);


	printf("Berikut Matriks Pertama : \n");
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K1; j++){
        	printf("%d\t", mat1[i][j]);
    	}
    	printf("\n");
	}


	printf("\nBerikut Matriks Kedua : \n");
	for(int i=0; i<B2; i++){
    	for(int j=0; j<K2; j++){
        	printf("%d\t", mat2[i][j]);
    	}
    	printf("\n");
	}




	printf("\nHasil Perkalian Dua Matriks :  \n");
	int matriks[B1][K2];
	for(int i=0; i<B1; i++){     	//kurang dari baris matriks a
    	for(int j=0; j<K2; j++){ 	//kurang dari kolom matriks b
        	matriks[i][j] = 0;
        	for(int k=0; k<B2; k++){ //kurang dari baris dari matriks b
            	matriks[i][j] += mat1[i][k]*mat2[k][j];
        	}
    	}
	}


	//shared memory
	key_t key = 1236;   //key shared memory yang akan dipakai
	int shmid = shmget(key, sizeof(int)*B1*K2, IPC_CREAT | 0666);   //id sharedmemory yg akan dipakai
	int *shmaddr = (int *)shmat(shmid, NULL, 0);	//variabel buat simpan isi perkalian matriks, sama aja kyk array satu dimensi


	// copy hasil operasi matriks ke shared memory
	int count = 0;
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	*(shmaddr + count) = matriks[i][j];
        	count++;
    	}
	}


	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	printf("%d\t", matriks[i][j]);
    	}
    	printf("\n");
	}
	//memberi waktu untuk mengrun program cinta dan sisop
	sleep(20);
   
	//detach lalu destroy shared memory
	shmdt((void *)shmaddr);
	shmctl(shmid, IPC_RMID, NULL);
	return 0;
}

Penjelasan program kalian :

```
int** Matrix(int baris, int kolom, int BatasAtas, int BatasBawah) {
	int** matriks = malloc(baris * sizeof(int*));
	srand(time(NULL));
	for (int n=0; n<baris; n++) {
    	matriks[n] = malloc(kolom * sizeof(int));
    	for (int m=0; m<kolom; m++) {
        	matriks[n][m] = (rand() % (BatasAtas - BatasBawah + 1)) + BatasBawah;
    	}
	}
	return matriks;
}

  - fungsi ini berguna untuk membuat pembentukan matriks 
  - fungsi ini menggunakan double pointer dalam pembentukan matriks
  - angka random di peroleh dari srand(time(NULL)) dan (rand() % (BatasAtas - BatasBawah + 1)) + BatasBawah



int matriks[B1][K2];
	for(int i=0; i<B1; i++){     	//kurang dari baris matriks a
    	for(int j=0; j<K2; j++){ 	//kurang dari kolom matriks b
        	matriks[i][j] = 0;
        	for(int k=0; k<B2; k++){ //kurang dari baris dari matriks b
            	matriks[i][j] += mat1[i][k]*mat2[k][j];
        	}
    	}
	}


kode diatas dimana perkalian matriks di simpan dalam array dua dimensi 

//shared memory
	key_t key = 1236;   //key shared memory yang akan dipakai
	int shmid = shmget(key, sizeof(int)*B1*K2, IPC_CREAT | 0666);   //id sharedmemory yg akan dipakai
	int *shmaddr = (int *)shmat(shmid, NULL, 0);	//variabel buat simpan isi perkalian matriks, sama aja kyk array satu dimensi

// copy hasil operasi matriks ke shared memory
	int count = 0;
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	*(shmaddr + count) = matriks[i][j];
        	count++;
    	}
	}
    sleep(20);

   -> key 1236 merupakan key yang di gunakan untuk shared memory
   -> int *shamdr digunakan untuk menyimpan hasil perkalian matriks
   -> sleep berguna untuk memberi waktu untuk menjalankan program cinta dan sisop


cinta :

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>


#define B1 4
#define K2 5


int multiply(int ang, int arr[], int num)
{
	int ans = 0;
	//melakukan perkalian setiap elemen array arr[] dengan ang (pakai bantuan hasil dan ans)
	for(int j=0; j<num; j++) { 	 
    	int hasil;
    	hasil = (arr[j]*ang) + ans;
    	//menyimpan digit terakhir dari hasil ke array arr[]
    	arr[j] = hasil%10;       	 
    	ans = hasil/10;    
	}
   
   //menginputkan ans ke array arr[] lalu menambah size array arr[]
  	 
	while(ans) {                 	 
    	arr[num] = ans%10;
    	ans/=10;
    	num++;
	}
	return num;
}


void *faktorial(void *args)  	//fungsi faktorial untuk menghitung nilai faktorial dan print hasilnya
{
	int arr[100];       	//maksimal output 100 digit (meskipun worstcase 48 digit)
   
	//ambil value dari argumen
	int n;
	n = *((int *)args);


	arr[0] = 1;
	int ans = 1;


	//melakukan faktorial
	for(int i = 2; i <= n; i++){
   	ans = multiply(i, arr, ans);
	}


	//print hasil faktorial
	for(int i = ans - 1; i >= 0; i--){
    	printf("%d", arr[i]);
	}
	return NULL;
}


int main()
{
	//buat menghitung execution time
	clock_t start, finish;
	double waktu;
	start = clock();


	int matriks[B1][K2];
	//untuk mengatur pengaksesan shared memory
	key_t key = 1236;
	int shmid = shmget(key, sizeof(int)*B1*K2, IPC_CREAT | 0666);
	int *shmaddr = (int *)shmat(shmid, NULL, 0);


	printf("hasil kali kedua matriks dari shared memory: \n");
	int ans = 0;
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	printf("%d\t", *(shmaddr + ans));
        	//memasukan ke matriks buat dipake itung faktorial
        	matriks[i][j]=*(shmaddr + ans);
        	ans++;
    	}
    	printf("\n");
	}


	//multithreading faktorial
	printf("\nBerikut ini adalah hasil faktorial (THREAD) perkalian matriks sebelumnya: \n");
	pthread_t tid[B1][K2];
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	pthread_create(&tid[i][j], NULL, faktorial, &matriks[i][j]);
        	//melakukan join di setiap thread nya
        	pthread_join(tid[i][j], NULL);
        	printf(" ");
    	}
	}
	printf("\n");


	shmdt((void *)shmaddr);


	//perhitungan waktu eksekusi telah selesai
	finish = clock();
	waktu = ((double) (finish - start)) / CLOCKS_PER_SEC;
	printf("\n\nWaktu eksekusi (execution time): %f detik\n", waktu);


	return 0;
}

Penjelasan : 

void *faktorial(void *args)  	//fungsi faktorial untuk menghitung nilai faktorial dan print hasilnya
{
	int arr[100];       	//maksimal output 100 digit (meskipun worstcase 48 digit)
   
	//ambil value dari argumen
	int n;
	n = *((int *)args);


	arr[0] = 1;
	int ans = 1;


	//melakukan faktorial
	for(int i = 2; i <= n; i++){
   	ans = multiply(i, arr, ans);
	}


	//print hasil faktorial
	for(int i = ans - 1; i >= 0; i--){
    	printf("%d", arr[i]);
	}
	return NULL;
}

    ->fungsi faktorial menggunakan void pointer bertujuan untuk menyesuaikan penggunaan multithreading ,sehingga argumennya juga harus berupa void pointer
    -> pada fungsi ini juga memerlukan fungsi multiply untuk melakukan perkalian 


	//multithreading faktorial
	printf("\nBerikut ini adalah hasil faktorial (THREAD) perkalian matriks sebelumnya: \n");
	pthread_t tid[B1][K2];
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	pthread_create(&tid[i][j], NULL, faktorial, &matriks[i][j]);
        	//melakukan join di setiap thread nya
        	pthread_join(tid[i][j], NULL);
        	printf(" ");
    	}
	}

    -> pthread_t tid[B1][K2] berguna menyimpan id dari setiap thread
    ->pthread_create(&tid[i][j], NULL, faktorial, &matriks[i][j]) berguna untuk melakukan faktorial setiap elemen matriks dengan tidnya masing masing
    ->pthread_join(tid[i][j], NULL) berguna untuk menjoin tiap thread agar hasil faktorial bisa urut


sisop

    
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>


#define B1 4
#define K2 5


int multiply(int ang, int arr[], int num)
{
	int ans = 0;
	//melakukan perkalian setiap elemen array arr[] dengan ang (pakai bantuan hasil dan ans)
	for(int j=0; j<num; j++) { 	 
    	int hasil;
    	hasil = (arr[j]*ang) + ans;
    	//menyimpan digit terakhir dari produk ke array arr[]
    	arr[j] = hasil%10;       	 
    	ans = hasil/10;    
	}
   
   //menginputkan ans ke array arr[] lalu menambah size array arr[]
  	 
	while(ans) {                 	 
    	arr[num] = ans%10;
    	ans/=10;
    	num++;
	}
	return num;
}


void fakt(int n)  	//fungsi faktorial untuk menghitung nilai faktorial dan print hasilnya
{
	//untuk menyimpan hasil output digit
	int arr[100];     	 


	arr[0] = 1;
	int ans = 1;


	//melakukan faktorial
	for(int num = 2; num <= n; num++){
    	ans = multiply(num, arr, ans);
	}


	//print hasil faktorial
	for(int i = ans - 1; i >= 0; i--){
    	printf("%d", arr[i]);
	}
}


int main()
{
	//buat menghitung execution time
   clock_t start, finish;
	double waktu;
	start = clock();


	int matriks[B1][K2];
	key_t key = 1236;
	int shmid = shmget(key, sizeof(int)*B1*K2, IPC_CREAT | 0666);
	int *shmaddr = (int *)shmat(shmid, NULL, 0);


	printf("hasil kali kedua matriks dari shared memory: \n");
	int ans = 0;
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	printf("%d\t", *(shmaddr + ans));
        	//memasukan ke matriks buat dipake itung faktorial
        	matriks[i][j]=*(shmaddr + ans);
        	ans++;
    	}
    	printf("\n");
	}


	//faktorial tanpa multithread
	printf("\nBerikut ini adalah hasil faktorial (NO-THREAD) perkalian matriks sebelumnya: \n");
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	fakt(matriks[i][j]);
        	printf(" ");
    	}
	}
	printf("\n");


	shmdt((void *)shmaddr);


	//perhitungan waktu eksekusi telah selesai
 	finish = clock();
	waktu = ((double) (finish - start)) / CLOCKS_PER_SEC;
	printf("\n\nWaktu eksekusi (execution time): %f detik\n", waktu);
   
	return 0;
}

Penjelasan : 

void fakt(int n)  	//fungsi faktorial untuk menghitung nilai faktorial dan print hasilnya
{
	//untuk menyimpan hasil output digit
	int arr[100];     	 


	arr[0] = 1;
	int ans = 1;


	//melakukan faktorial
	for(int num = 2; num <= n; num++){
    	ans = multiply(num, arr, ans);
	}


	//print hasil faktorial
	for(int i = ans - 1; i >= 0; i--){
    	printf("%d", arr[i]);
	}
}

    -> dimana fungsi fakt disini bertipe void

sehingga dalam pemanggilannya ada sedikit perubahan :


	//faktorial tanpa multithread
	printf("\nBerikut ini adalah hasil faktorial (NO-THREAD) perkalian matriks sebelumnya: \n");
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	fakt(matriks[i][j]);
        	printf(" ");
    	}
	}

    -> pemanggilan fungsi cukup sederhana yaitu cukup fakt(matriks[i][j]);
    -> selain itu sama seperti program cinta

Untuk melakukan perbandingan antara program cinta dan sisop disini menggunakan eksekusi time, untuk program nya sebagai berikut:

    //buat menghitung execution time
   clock_t start, finish;
	double waktu;
	start = clock();


	//perhitungan waktu eksekusi telah selesai
 	finish = clock();
	waktu = ((double) (finish - start)) / CLOCKS_PER_SEC;
	printf("\n\nWaktu eksekusi (execution time): %f detik\n", waktu);
``` 
-------------
## Nomor 3

Soal meminta kita untuk membuat sistem stream(receiver) stream.c dengan user(multiple sender dengan idetifier) user.c menggunakan message queue. user mengirimkan perintah berupa _string_ ke sistem dan semua aktivitas sesuai perintah dijalankan oleh sistem(stream)

deklarasi:

```
#include <stdio.h>
#include <stdlib.h>
#include <json-c/json.h>
#include <string.h>
#include <ctype.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>

struct mesg_buffer
{
    long mesg_type;
    char mesg_text[100];
    char song[100];
    pid_t id;
} message;
```

struct digunakan untuk menyimpan variabel yang digunakan untuk message queue

```
key_t key;
    int msgid;
    key = ftok("progfile", 65);
    msgid = msgget(key, 0666 | IPC_CREAT);
    message.mesg_type = 1;
    while (1)
    {
        printf("Input Command : ");
        scanf("%s", message.mesg_text);
        strcpy(message.song, "");

        pid_t user_pid = getpid();
        message.id = user_pid;

        if (strcasecmp(message.mesg_text, "PLAY") == 0)
        {
            char temp;
            scanf("%c", &temp);
            scanf("%[^\n]", message.song);
        }
        else if (strcasecmp(message.mesg_text, "ADD") == 0)
        {
            char temp;
            scanf("%c", &temp);
            scanf("%[^\n]", message.song);
        }
        msgsnd(msgid, &message, sizeof(message), 0);
        printf("command : %s %s\n", message.mesg_text, message.song);
        // exit case
        if (strcasecmp(message.mesg_text, "EXIT") == 0)
        {
            return 0;
        }
    }
```

kode diatas dari user.c dan berfungsi untuk menerima input dari user dan mengirimkan perintah tersebut kepada stream.c menggunakan message queue

selanjutnya stream.c menerima input dari user kemudian membuat semaphore untuk membatasi akses dari user sebanyak 2

```
key_t key;
    int msgid;
    pid_t users[2];
    int userCount = 0;

    key = ftok("progfile", 65);

    msgid = msgget(key, 0666 | IPC_CREAT);

    sem_t *sem;
    sem = sem_open("/semaphore", O_CREAT, 0644, 2);

    while (1)
    {
        msgrcv(msgid, &message, sizeof(message), 1, 0);
        printf("Action: %s\n", message.mesg_text);

        int found_user = 0;
        for (int i = 0; i < userCount; i++)
        {
            if (users[i] == message.id)
            {
                found_user = 1;
                break;
            }
        }
        if (!found_user && userCount == 2)
        {
            printf("STREAM SYSTEM OVERLOAD: User %d cannot send commands at the moment.\n", message.id);
            continue;
        }
        if (!found_user)
        {
            if (sem_wait(sem) == -1)
            {
                perror("sem_wait");
                exit(1);
            }
            users[userCount++] = message.id;
        }

        if (strcasecmp(message.mesg_text, "DECRYPT") == 0)
        {
            decrypt();
            sort();
            printf("deryption success\n");
        }
        else if (strcasecmp(message.mesg_text, "LIST") == 0)
        {
            list();
        }
        else if (strcasecmp(message.mesg_text, "PLAY") == 0)
        {
            play(message.song);
        }
        else if (strcasecmp(message.mesg_text, "ADD") == 0)
        {
            add(message.song);
            sort();
            printf("Song added\n");
        }

        else if (strcasecmp(message.mesg_text, "EXIT") == 0)
        {
            int found_user = 0;
            for (int i = 0; i < userCount; i++)
            {
                if (users[i] == message.id)
                {
                    found_user = 1;

                    for (int j = i; j < userCount - 1; j++)
                    {
                        users[j] = users[j + 1];
                    }
                    userCount--;
                    break;
                }
            }

            if (found_user)
            {
                printf("User %d has quit.\n", message.id);
            }
            else
            {
                printf("User %d is not active.\n", message.id);
            }
        }
        else if (strcasecmp(message.mesg_text, "SHUTDOWN") == 0)
        {
            if (msgctl(msgid, IPC_RMID, NULL) == -1)
            {
                perror("msgctl");
                exit(1);
            }

            if (sem_destroy(sem) == -1)
            {
                perror("sem_destroy");
                exit(1);
            }

            exit(0);
        }
        else
        {
            printf("Unknown Command\n");
        }
```

ada 4 fungsi yang digunakan yaitu decrypt(), list(), add(), dan play().
fungsi decrypt akan menggunakan library json-c yang digunakan untuk membaca json file kemudian akan di decrypt sesuai dengan metode yang terdapat pada file json.

```
sudo apt-get install libjson-c-dev
```

digunakan untuk instalasi library json-c

```
sudo apt-get install coreutils
```

digunakan untuk menginstall command line untuk decode base64

```
sudo apt-get install xxd
```

digunakan untuk decode hex.  
Lalu file stream.c dapat dicompile dengan perintah berikut

```
gcc stream.c -o stream -ljson-c
```

Pada saat melakukan perintah decrypt, program akan membaca dan mendecrypt string dari file json keudian di output menggunakan fprintf() ke dalam file playlist.txt kemudian akan di sort sesuai alphabet.  
Perintah list menggunakan commandline system() menggunakan cat untuk menampilkan list dari file playlist.txt.  
perintah play menerima 1 buah string tambahan dari user yang digunakan sebagai kata kunci untuk lagu yang ingin di play.  
perintah add juga menerima 1 buah string untuk judul lagu dan penyanyi yang akan ditambahkan ke dalam playlist.txt

untuk implementasi semaphore kami sedikit mengalami kesulitan. namun pada saat revisi program ini sudah ditambahkan fitur tersebut dimana hanya maksimal 2 process dari user.c yang dapat mengakses stream.c jika ada yang lebih makan akan mengeluarkan stream system overload

-------------
# No 4
## Unzip.c

Untuk program unzip.c, proses unzip dan mengunduh dilakukan sebagai berikut :
- Mengunduh : Menggunakan libcurl
- Unzip     : Menggunakan `execv` dan perintah `unzip`.
 

### 1. Includes dan fungsi unzip
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//For error codes
#include <errno.h>
#include <sys/stat.h>
//For wait
#include <sys/wait.h>

#include <curl/curl.h>

//Unzip function
int unzipDir(char name[]){
    //Calls exec in child process
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (pid == 0) {
        printf("Unzipping %s (%d)\n", name, getpid());
        char *argv[] = {"zip", "-q", name, NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    else{
        int status;
        do {
            status = wait(NULL);
            if(status == -1 && errno != ECHILD) {
                perror("Error during wait()");
                abort();
            }
        } while (status > 0);
    }

    return(0);
}
```
Fungsi `unzipDir` :
- Input : Path file yang diunzip
- Proses :
    - Membuat child proses
    - Child proses memanggil fungsi unzip menggunakan argv

### 2. Fungsi write_data
```
static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
  size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
  return written;
}
```
Merupakan fungsi yang digunakan `curl` untuk menuliskan data yang diunduh ke dalam file.

### 3. Fungsi main
```
int main(int argc, char *argv[])
{
    CURL *curl_handle;
    static const char *pagefilename = "hehe.zip";
    FILE *pagefile;
    

    
    curl_global_init(CURL_GLOBAL_ALL);
    
    /* init the curl session */
    curl_handle = curl_easy_init();
    
    /* set URL to get here */
    curl_easy_setopt(curl_handle, CURLOPT_URL, "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp");
    
    /* Switch on full protocol/debug output while testing */
    curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);
    
    /* disable progress meter, set to 0L to enable it */
    curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);

      /* Enables redirects */
    curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1L);

      /* Change user agent to firefox, avoids "error 1020" due to curl being blocked*/
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/111.0");

    /* send all data to this function  */
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);
    
    /* open the file */
    pagefile = fopen(pagefilename, "wb");
    if(pagefile) {
    
        /* write the page body to this file handle */
        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, pagefile);
    
        /* get it! */
        curl_easy_perform(curl_handle);
    
        /* close the header file */
        fclose(pagefile);
    }
    
    /* cleanup curl stuff */
    curl_easy_cleanup(curl_handle);
    
    curl_global_cleanup();

    /*-------------------------------------------------*/

    unzipDir("./hehe.zip");
    
    return 0;
}
```
Proses :
- Menyiapkan path file berisi data hasil unduhan
- Memanggil fungsi untuk inisialisasi dan setup pengaturan `curl`. Beberapa diantaranya seperti :      
    - Mengatur URL file yang diunduh.
    - Mengatur agar `curl` mengikuti redirects.
    - Mengatur agar useragent menggunakan browser lain seperti `firefox`
- Curl mengunduh lalu menuliskan data hasil unduhan ke dalam file.
- Curl cleanup.
- Mengunzip file hasil unduhan.
 

 ## Categorize.c
 ### 1. Includes dan Defines
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
//For error codes
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
//For threads
#include <pthread.h>
//To access folder contents
#include <dirent.h>
//For nanosleep & time for log
#include <time.h>

#define MAX_LEN 50
#define CATNAME "categorized"
//Amount of storage folders (amount of extentions + "other")
#define folders 8
//Location of log
#define log_path "./log.txt"

//Debug test
#define NORMAL_COLOR  "\x1B[0m"
#define GREEN  "\x1B[32m"
#define BLUE  "\x1B[34m"
#define RED "\x1B[31m"
#define YELLOW "\x1B[33m"
#define MAGENTA "\x1B[36m"
```
Terdapat beberapa define :
- MAX_LEN   : Panjang maksimal string menyimpan baris saat membaca `extensions.txt` dan `max.txt`.
- CATNAME   : Nama folder yang menyimpan folder-folder extentions
- folders   : Jumlah folders extentions + others
- log_path  : Path file log
- Terdapat pula kode-kode warna yang digunakan untuk warna pesan di terminal.

### 2. Inisialisasi scope global
```
//Initializes MutEx
pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex3 = PTHREAD_MUTEX_INITIALIZER;

//Init values for sleep

// struct timespec {
//     time_t tv_sec;        /* seconds */
//     long   tv_nsec;       /* nanoseconds */
// };

struct timespec tenth, test = {0, 999};


//Struct to store amount of files in each folder
struct folderFiles{
    char name[10];
    int amount;
    int foldAmount;
};

//Global structs so every thread can access it
struct folderFiles fileAmount[folders];
int max; //Max files per folder

//Struct to dynamically store and add t_id for each folder
typedef struct tidLinked{
    pthread_t t_id;
    struct tidLinked *next;
}tidL;
```
Terdapat beberapa hal yang diinisialisasi dengan scope global :
- Inisialisasi mutex
- Inisialisasi struct `timespec` untuk fungsi `nanosleep()`.
- Inisialisasi struct `folderFiles` untuk menyimpan jumlah file + folder per ekstensi (seperti yang ditulis dalam `extentions.txt`).
- Membuat global `folderFiles` array dan int menyimpan max file per folder.
- Membuat struct `tidLinked` untuk menyimpan secara dinamis t_id secara linkedlist.

### 3. Fungsi linkedlist `tidLinked`
```
void init(tidL *list){
    list->next = NULL;
    list->t_id;
}

pthread_t * add(tidL *cur){
    tidL *new_node = (tidL*)calloc(1, sizeof(tidL));

    cur->next = new_node;
    new_node->next = NULL;
    return &(new_node->t_id);
}

void clean_up(tidL *list){
    tidL *temp = NULL;

    while(list != NULL){
        temp = list;
        list = list->next;
        pthread_join(temp->t_id, NULL);
        //free(temp);
    }
}
```
Untuk membuat linkedlist, terdapat beberapa fungsi :
- `init` : Inisialisasi head node (tidak berisi data apapun)
- `add` : Menambah node ke dalam linkedlist yang berisi t_id yang baru dibuat. Mereturn alaman variabel `t_id` karena digunakan dalam fungsi `pthread_create`.
- `clean_up` : Fungsi yang membuat thread menunggu hingga semua thread yang idnya berada di dalam linkedlist selesai. 

### 4. to_lower_case dan logging
```
//Function to convert string to lowercase
void to_lower_case(char * in){
    for(int i = 0; in[i]; i++){
        in[i] = tolower(in[i]);
    }
}
//Function to write log

void logging(int type, char arg1[], char arg2[], char arg3[]){
    pthread_mutex_lock(&mutex1);
    FILE * log = fopen(log_path, "a");

    char log_msg[500];
    char time_str[20];
    //Get time
    time_t rawtime;   
    time ( &rawtime );
    struct tm *timeinfo = localtime ( &rawtime );
    strftime(time_str, 20, "%d-%m-%Y %T", timeinfo);

    if(type == 1){
        //Accessed
        fprintf(log, "%s ACCESSED %s\n", time_str, (arg1 + 2));
    }
    else if(type == 2){
        //Moved
        fprintf(log, "%s MOVED %s file : %.*s > %.*s\n", time_str, arg1, (int) strcspn((arg2+3), "\'"),(arg2 + 3), (int) strcspn((arg3 + 3), "\""), (arg3 + 3));
    }
    else if(type == 3){
        //Made
        fprintf(log, "%s MADE %s\n", time_str, arg1);
    }
    
    pthread_mutex_unlock(&mutex1);
    fclose(log);
}
```
- `to_lower_case` : Digunakan untuk mengubah semua huruf dalam suatu string menjadi huruf kecil.
- `logging` : Fungsi yang digunakan untuk menuliskan log sesuai format yang diberikan dalam soal. :
    - Membuka file `log.txt`
    - Menggunakan `time_t` dan `tm`, membuat string berisi waktu dan tanggal sesuai format.
    - Mencetak pesan sesuai variabel `type` :
        - 1 = ACCESSED
        - 2 = MOVED
        - 3 = MADE
    
    Fungsi `logging` juga menggunakan mutex `mutex1` agar hanya 1 thread yang bisa menulis log dalam satu waktu.

### 5. createDir
```
void createDir(char name[]){
    int check = mkdir(name, 0777);

    if (!check){
        printf("Directory %s created\n", name);
        logging(3, name, NULL, NULL);
    }
    else{
        printf("ERROR : \"%s\"", name);
        exit(1);
    }
}
```
Fungsi ini memanggil `mkdir` untuk membuat folder sesuai path yang diberikan. Fungsi akan memanggil `logging()` dengan tipe ke-3 (MADE).

### 6. Selection sorting functions
```
//Function to sort ascendingly
void swap(struct folderFiles* xp, struct folderFiles* yp)
{
    struct folderFiles temp = *xp;
    *xp = *yp;
    *yp = temp;
}

// Function to perform Selection Sort
void selectionSort(struct folderFiles arr[], int n)
{
    int i, j, min_idx;

    // One by one move boundary of unsorted subarray
    for (i = 0; i < n - 1; i++) {

        // Find the minimum element in unsorted array
        min_idx = i;
        for (j = i + 1; j < n; j++){
            if (strcmp(arr[j].name, arr[min_idx].name) < 0){
                min_idx = j;    
            }
        }

        // Swap the found minimum element
        // with the first element
        swap(&arr[min_idx], &arr[i]);
    
```
Berikut adalah fungsi-fungsi yang digunakan untuk menyortir struct array `folderFiles` agar terurut secara *ascending* :
- `swap()` : Fungsi untuk menukar posisi 2 struct.
- `selectionSort()` : Fungsi yang menyortir menggunakan metode *selection sort*. Hasil dari `strcmp()` menggunakan nama extension digunakan sebagai pembanding agar terurut secara *ascending*.

### 7. Thread function
Berikut adalah fungsi utama yang digunakan thread untuk menyortir semua file di dalam directory `files` dan semua subdirectory di dalamnya.
```
void *recursive(void *args) {

    char dirPath[200];//Folder relative path
    strcpy(dirPath, (char*)args);

    

    printf("%sSTART > Thread managing \"%s\"\n", YELLOW, dirPath);

    //Initializes linked list to store all subthread's id
    tidL tid_list;
    init(&tid_list);

    DIR * d = opendir(dirPath); // open the path
    if(d==NULL) {
        printf("%sERROR : Unable to open %s\n", RED, dirPath);
        return NULL;
        } // if was not able, return
    struct dirent * dir; // for the directory entries
    char f_name[200];//2 uses
    char next_path[200]; // here I am using sprintf which is safer than strcat
    char command[200];
```
- Argumen utama dari fungsi ini nantinya berisi directory tempat thread bekerja (karena setiap akses directory dan subdirectory harus membuat thread baru).
- Thread menyimpan path directory tersebut dalam `dirPath`.
- Setelah mencetak pesan, thread membuat linkedlist untuk menyimpan t_id setiap thread yang dibuat untuk subdirectory dalam folder tempat kerjanya.
- Thread lalu membuka informasi directorynya menggunakan `opendir()`.
- Thread juga menyiapkan beberapa variabel :
    - `dir` : Pointer ke struct dirent, berisi informasi foldernya saat ini.
    - `f_name`, `next_path`, char array untuk menyimpan potongan path dan nama folder ketika membuat path baru nantinya.
    - `command`, char array untuk menyimpan perintah. Akan digunakan untuk menyimpan perintah `mv` sebelum dieksekusi menggunakan `system()`.

### LOOP UTAMA
```
    while ((dir = readdir(d)) != NULL) // if we were able to read somehting from the directory
    {
```
Program akan membaca informasi setiap file dan subdirectory didalam directory, akan terjadi 2 hasil tergantung apakah terbaca file atau subdirectory.
- **File**
```
        if(dir-> d_type != DT_DIR){ // if the type is not directory just print it with blue color

            
            
            sprintf(f_name, "%s", dir->d_name);

            //Retrieves extention (.{extention})
            char * ext; //The extention
            char * token = strtok(f_name, ".");
            ext = token;
            // loop through the string to extract all other tokens
            while( token != NULL ) {
                ext = token;
                token = strtok(NULL, ".");
            }
            char d_path[200]; // here I am using sprintf which is safer than strcat
            char dest_path[200];
            sprintf(d_path, "\'%s/%s\'", dirPath, dir->d_name);
            

            //Moves file depending on it's extension
            int other_ext = 0;
            int x = 0;
            int oth = 0;

            //Compares file with every extention in extensions.txt
            char ext2[50];
            sprintf(ext2, "%s", ext);
            to_lower_case(ext2);
            while(x < folders){
                if(!oth && (strcmp(fileAmount[x].name, "other") == 0)){
                    oth = x;
                    continue;
                }
                if(strcmp(fileAmount[x].name, ext2) == 0 ){
                    other_ext = 1;
                    break;
                }
                x++;
            }
```
Jika dibaca file :
- Menyimpan nama file (termasuk ekstensinya) ke dalam `f_name`.
- Menggunakan strtok dengan delimiter "." agar mendapatkan ekstensi.
- Membangun path file tersebut ke dalam `d_path`.
- Ekstesi dicopy ke dalam `ext2`. 
- Setelah diubah menjadi lowercase menggunakan `to_lower_case`, membandingkan ekstensi file (`ext2`) dengan ekstensi yang ada di dalam array `fileAmount` :
    - **Jika ekstensi ada di dalam `extentions.txt`**

        ```
                //Moves file
                if (other_ext)
                {
                    //LOCK
                    
                    //If current folder is full
                    if(fileAmount[x].amount > 0 && (fileAmount[x].amount % max) == 0){
                        fileAmount[x].foldAmount += 1;
                        sprintf(dest_path, "%s/%s (%d)", CATNAME, fileAmount[x].name, fileAmount[x].foldAmount);
                        createDir(dest_path);
                    }
                    //Generates destination path (based on amount of folder)
                    if(fileAmount[x].foldAmount > 1 && fileAmount[x].amount > 9){
                        sprintf(dest_path, "\"./%s/%s (%d)/%s\"", CATNAME, fileAmount[x].name, fileAmount[x].foldAmount,dir->d_name);
                    }
                    else if(fileAmount[x].amount < 10){
                        sprintf(dest_path, "\"./%s/%s/%s\"", CATNAME, fileAmount[x].name, dir->d_name);
                    }
                    //char * commnd = malloc(100);
                    
                    
                    
                    printf("%sMOVING : %s -> %s\n",MAGENTA, d_path, dest_path);
                    
                    sprintf(command, "mv %s %s", d_path, dest_path);

                    if(!system(command)){
                        fileAmount[x].amount += 1; 
                        logging(2, ext2, d_path, dest_path);
                    }
                    //nanosleep(&tenth, &test);

                    //free(commnd);
                    //UNLOCK
                    //
                }
        ```
        - Memeriksa apakah folder ekstensi tersebut penuh (sesuai jumlah `max.txt`). Jika iya membuat folder baru.
        - Apakah sudah ada lebih dari 1 folder?
            - Ya, maka buat path tujuan dengan folder tujuan "[extention] ([jumlah folder])".
            - Tidak, buat path tujuan dengan folder awal.
        - Cetak pesan lalu buat perintah mv dalam `command`.
        - Eksekusi perintah mv, **hanya jika perintah berhasil (return value `mv` = `command` = 0)** menambah jumlah file dalam `fileAmount` dan menuliskan log (`logging()`).

    - **Jika ekstensi lain**
        ```
                else{
                //LOCK
                
                sprintf(dest_path, "\'./%s/other/%s\'", CATNAME, dir->d_name);
                
                printf("%sMOVING : %s -> %s\n",MAGENTA, d_path, dest_path);

                //pthread_mutex_lock(&mutex2);
                //char * command = malloc(100);
                
                sprintf(command, "mv %s %s", d_path, dest_path);

                printf("%s\n", command);
                
                if(!system(command)){
                    fileAmount[oth].amount += 1;
                    logging(2, ext2, d_path, dest_path);
                }
                //free(command);
                //UNLOCK
                //nanosleep(&tenth, &test);

                //pthread_mutex_unlock(&mutex2);
            }
        ```
    - Membuat path tujuan dengan folder tujuan other.
    - Setelah mencetak pesan, membuat perintah `mv` ke dalam `command`.
    - Eksekusi perintah mv, **hanya jika perintah berhasil (return value `mv` = `command` = 0)** menambah jumlah file dalam `fileAmount` dan menuliskan log (`logging()`).

- **Subdirectory**
    ```
            else
            if(dir -> d_type == DT_DIR && strcmp(dir->d_name,".")!=0 && strcmp(dir->d_name,"..")!=0 ) // if it is a directory
            {
                
                printf("%s %s is creating thread for %s\n",GREEN, dirPath, dir->d_name); // print its name in green
                
                pthread_mutex_lock(&mutex3);
                sprintf(next_path, "%s/%s", dirPath, dir->d_name);

                //printf("CREATE THREAD %s : %d\n", dir->d_name, pthread_create(add(&tid_list), NULL, &recursive, (void *)next_path));// recall with the new path
                pthread_create(add(&tid_list), NULL, &recursive, (void *)next_path);
                logging(1, next_path, NULL, NULL);
                //nanosleep(&tenth, &test);
                //nanosleep(&tenth, &test);
                //sleep(1);
                
                pthread_mutex_unlock(&mutex3);
            }
    ```
    - Mencetak pesan (mutex merupakan sisa debugging yang belum dihapus, namun karena program sudah berjalan dengan benar maka dibiarkan).
    - Membuat path subdirectory tersebut.
    - Memanggil `pthread_create` dengan argumen :
        - `add(&tid_list), agar pthread_create menyimpan t_id ke dalam linkedlist.
        - Fungsi ini
        - Path subdirectory yang dikerjakan thread baru ini.
    - Mencetak log.

Ketika thread selesai, maka akan mencetak pesan dan menunggu semua thread yang dibuatnya selesai. Jika sudah maka akan menutup directory yang dibuka dan keluar.
### 8. Main
```
int main(){
    pthread_mutex_init(&mutex1, NULL);
    pthread_mutex_init(&mutex2, NULL);
    pthread_mutex_init(&mutex3, NULL);

    FILE * extentions = fopen("./extensions.txt", "r");
    FILE * maxFile = fopen("./max.txt", "r");
```

- Menginisialisasi mutex
- Membuka file `extentions.txt` dan `max.txt`.
- Membuat semua folder (termasuk "other") sesuai ekstensi di dalam `extentions.txt`.
    ```
    //Untuk menyimpan jumlah file dalam setiap folder hasil, digunakan struct array.

    int count = 0; //Menghitung index

    char catFol[200];
    strcpy(catFol, CATNAME);
    //Creates all storage folders
    createDir(catFol);
    createDir(strcat(catFol, "/other"));
    strcpy(fileAmount[count].name, "other");
    fileAmount[count].amount = 0;
    fileAmount[count].foldAmount = 1;
    count++;
    strcpy(catFol, CATNAME);
    char buffer[MAX_LEN];
    while (fgets(buffer, MAX_LEN, extentions))
    {
        // Remove trailing newline
        buffer[strcspn(buffer, "\n\r")] = 0;
        strcpy(fileAmount[count].name, buffer);
        fileAmount[count].amount = 0;
        fileAmount[count].foldAmount = 1;
        count++;

        strcat(catFol, "/");
        strcat(catFol, buffer);
        createDir(catFol);
        strcpy(catFol, CATNAME);
    }
    ```
    - Membuat path lalu folder others.
    - Menginisialisasi isi `amount` (jumlah file dalam folder-folder ekstensi ini) dan `foldAmount` (jumlah folder) sebagai 0 dan 1. 
    - Membuat buffer lalu membaca isi `extentions.txt`. Untuk setiap ekstensi :
        - Menuliskan nama ekstensi dalam `fileAmount`
        - Inisialisasi isi `amount` dan `foldAmount` sebagai 0 dan 1.
        - Membuat path dan membuat folder awal.
        - Overwrite char array berisi path folder agar bisa digunakan ulang.
    
```
    //Stores max file in folder
    fgets(buffer, MAX_LEN, maxFile);
    max = atoi(buffer);
    //Sorts order of extention names 
    selectionSort(fileAmount, folders);
```
- Membacara isi `max.txt` dan menyimpannya sebagai integer di dalam variabel global `max`.
- Mengurutkan ekstensi menjadi *ascending*. (`selectionSort()`).
```
    //Starts sorting process
    pthread_t t_id;
    char target[] = "./files";
    //pthread_create(&t_id, NULL, &run, (void *)test);
    pthread_create(&t_id, NULL, &recursive, (void *)target);

    //pthread_join(t_id, NULL);
    sleep(2);
    pthread_join(t_id, NULL);
    
    pthread_mutex_destroy(&mutex1);
    pthread_mutex_destroy(&mutex2);
    pthread_mutex_destroy(&mutex3);
```
- Menyiapkan path folder target yang diproses.
- Membuat thread awal untuk memulai proses.
- Sleep 2 detik agar thread awal tidak selesai sebelum proses selesai lalu join thread tersebut.
- Menghapus mutex
```

    printf("\n\n");
    for(int k = 0; k < folders; k++){
        printf("%s : %d\n", fileAmount[k].name, fileAmount[k].amount);
    }

    fclose(extentions);
    fclose(maxFile);
    return 0;
```
- Mencetak banyak file per ekstensi.
- Menutup file pointer dan selesai.

### MASALAH YANG DIHADAPI
- Fungsi thread : Saat di debug, sepertinya `command` dan `next_path` bisa memiliki alamat memori yang sama. Hal ini berarti jika `pthread_create()` memakan waktu lama, thread akan lanjut memproses file sehingga nilai `next_path` berubah. Hal ini berujung pada thread gagal mengelola subdirectory. || Solusi : Menambah delay (`nanosleep()`).
- Fungsi thread : Pertama kali menemui stak smashing karena ukuran char array terlalu kecil.
- Jika ada karakter khusus yang harus diescape untuk fungsi `mv` bekerja. || Solusi : String diberi tanda (`).
- Main thread keluar sebelum proses selesai. || Delay di main.
- Core dump terkadang terjadi. || Delay diatur lamanya.
- Terkadang, 1 kali panggilan `pthread_create()` menghasilkan 2 thread dengan argumen yang sama. || Memastikan panggilan log hanya sekali agar tidak ada duplikat dan memeriksa apakah `mv` berhasil atau tidak. 
## logchecker.c
### 1. Deklarasi
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#define MAX_LEN 256
#define MAX_FOL 100

//Struct to store amount of files in each folder
struct cell{
    char name[10];
    int amount;
};

struct cell folAmount[MAX_FOL];
struct cell extAmount[MAX_FOL];
```
- MAX_LEN : Panjang maksimal setiap entry log.
- MAX_FOL : Jumlah maksimal informasi folder yang bisa disimpan. (Folder-folder di dalam `categorized`).
- struct `cell` : Menyimpan informasi per :
    - `folAmount` : Jumlah file per folder.
    - `extAmount` : Jumlah file per ekstensi.

### 2. Fungsi pendukung
```
int getIndex(char * k, struct cell * arr){
    for(int x = 0; x < 100; x++){
        if(strcmp(k, (arr+x)->name) == 0){
            return x;
        }
    }
    return -1;
}

//Function to sort ascendingly
void swap(struct cell* xp, struct cell* yp)
{
    struct cell temp = *xp;
    *xp = *yp;
    *yp = temp;
}

// Function to perform Selection Sort
void selectionSort(struct cell arr[], int n)
{
    int i, j, min_idx;

    // One by one move boundary of unsorted subarray
    for (i = 0; i < n - 1; i++) {

        // Find the minimum element in unsorted array
        min_idx = i;
        for (j = i + 1; j < n; j++){
            
            if (strcmp(arr[j].name, arr[min_idx].name) < 0){
                min_idx = j;    
            }
        }

        // Swap the found minimum element
        // with the first element
        if(min_idx != i)
            swap(&arr[min_idx], &arr[i]);
    }
}
```
- `getIndex()` : Fungsi yang diberikan string, mencari index struct `cell` yang namanya sama.
- `swap()` : Fungsi untuk menukan posisi 2 struct.
- `selectionSort()` : Fungsi untuk mengurutkan array `cell` menjadi *ascending* sesuai nama per struct.

### 3. Main
```
    //Variables that stores data
    int acc = 0;
    int countExt = 0;
    int countFold = 0;


    FILE* fp;
    fp = fopen("log.txt", "r");
    if (fp == NULL) {
      perror("Failed: ");
      return 1;
    }

    char fileName[MAX_LEN]; //Filename
    char buffer[MAX_LEN];
    char buffer2[MAX_LEN]; //Used to get dest file path
```
- Mendeklarasi variabel counter.
- Membuka file log.
- Mendeklarasi char array untuk menyimpan string-string yang dibutuhan.
```
    while (fgets(buffer, MAX_LEN, fp))
    {
        strcpy(buffer2, buffer);
        // Remove trailing newline
        buffer[strcspn(buffer, "\n\r")] = 0;
        //Splits string
        char * token = strtok(buffer, " ");
        token = strtok(NULL, " ");
        token = strtok(NULL, " "); //Log type
```
- Untuk setiap baris `log.txt` :
    - Setelah menyimpan baris ke dalam `buffer2`, mendapatkan substring jenis log baris tersebut menggunakan strtok.
    - Memproses tergantung jenis log :
        - ACCESSED
            ```
            if(strcmp(token, "ACCESSED") == 0){
                acc++;
            }
            ```
            - Hanya menambah jumlah folder yang diakses (`acc`).
        - MADE : Menambah nama folder ke dalam array `folAmount` dan menambah jumlah folder `countFold`.
            ```
            else if(strcmp(token, "MADE") == 0){
                //Adds folder to list (ascending)
                //Reads created folder path
                token = strtok(NULL, " ");
                char * num = strtok(NULL, " ");
                strcpy(fileName, token);
                if(num && strlen(num) > 0){
                    
                    strcat(fileName, " ");
                    strcat(fileName, num);
                }
                
                token = strtok(fileName, "/");
                char * token2 = strtok(NULL, "/");
                if(token2){
                    strcpy(fileName, token2);
                }
                strcpy(folAmount[countFold].name, fileName);
                
                //printf("CREATED : %s\n", folAmount[countFold].name);
                countFold++;
            }
            ```
            - Memotong baris untuk mendapatkan path folder yang dibuat.
            - Jika folder ini bukan folder pertama, mendapatkan angka di nama folder (`(x)`).
            - Jika bukan folder `categorized`, potong path untuk mendapatkan nama foldernya saja.
            - Menyimpan nama folder di dalam `folAmount`.
        - MOVED
            ```
            else if(strcmp(token, "MOVED") == 0){
                //Process file extention
                token = strtok(NULL, " "); //File type
                int ext_id, fold_id;
                ext_id = getIndex(token, extAmount);
                if((countExt < MAX_FOL) && (ext_id < 0)){
                    //printf("New extention : %s\n", token);
                    strcpy(extAmount[countExt].name, token);
                    ext_id = countExt;
                    countExt++;
                    //printf("MOVED %s\n", token);
                    strcpy(fileName, token);
                }
                extAmount[ext_id].amount++;

                //Process folder (destination)
                token = strtok(buffer2, ">");
                token = strtok(NULL, ">");
                char * destFol = strtok(token, "/"); 
                destFol = strtok(NULL, "/"); 
                //strcpy(fileName, destFol);
                fold_id = getIndex(destFol, folAmount);
                folAmount[fold_id].amount++;

                //Add to categorized
                folAmount[getIndex("categorized", folAmount)].amount++;
            }
            ```
            - Mendapatkan ekstensi file tersebut.
            - Jika belum ada di dalam `extAmount`, ditambahkan informasinya.
            - Menambahkan jumlah file dengan ekstensi tersebut di `extAmount`.
            - Memotong `buffer2` untuk mendapatkan path, lalu nama folder tujuan file.
            - Menambah jumlah file di folder tujuan tersebut di `folAmount`.
```

    }

    printf("ACCESSED : %d\n", acc);
    printf("--------------------------------------------\n");
    selectionSort(folAmount, MAX_FOL);
    selectionSort(extAmount, MAX_FOL);
    //Test for stored folders

    printf("SORTED FOLDER + AMOUNT\n");
    for(int x = 0; x < MAX_FOL; x++){
        if(strcmp(folAmount[x].name, "")==0)
            continue;

        printf("%s : %d\n", folAmount[x].name, folAmount[x].amount);
    }
    printf("--------------------------------------------\n");
    // //Test for stored folders
    printf("SORTED EXT + AMOUNT\n");
    for(int x = 0; x < MAX_FOL; x++){
        if(strcmp(extAmount[x].name, "")==0)
            continue;
        printf("%s : %d\n", extAmount[x].name, extAmount[x].amount);
    }

    fclose(fp);
    return 0;
}
```
- Mencetak jumlah folder yang diakses.
- Melakukan sorting untuk `folAmount` dan `extAmount`
- Mencetak informasi file di dalam semua folder dan jumlah file per ekstensi. Melewati indeks array tanpa nama jika ukuran array lebih besar dari jumlah ekstensi/folder.
- Menutup file pointer dan selesai.
