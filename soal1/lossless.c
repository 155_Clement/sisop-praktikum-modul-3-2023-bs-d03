#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// For toupper
#include <ctype.h>
// For child process
#include <sys/types.h>
#include <sys/wait.h>

#define CHAR_AM 26

/*
ANALYSIS

parent :
1. Baca file
2. ARRAY : Hitung frekuensi kemunculan setiap huruf (FUNCTION)
3. SEND TO CHILD
------
6. DECOMPRESSION menggunakan huffman tree (FUNCTION)
7. Compare bitsize between original file + compressed.


child :
4. COMPRESSION menggunakan frekuensi kemunculan huruf (FUNCTION)
5. Huffman tree : SEND TO PARENT
*/

// Store character frequency
typedef struct charFreq
{
    char ch;
    int freq;
} cF;

// geeksforgeeks (Huffman tree code)
//  Worst case tree depth = N-1 (N = amount of characters encoded)
#define MAX_TREE_HT 25 

// A Huffman tree node
struct MinHeapNode
{
    //Both character and frequency
    cF data;

    // Left and right child of this node
    struct MinHeapNode *left, *right;
};

// A Min Heap:  Collection of
// min-heap (or Huffman tree) nodes
struct MinHeap
{

    // Current size of min heap
    unsigned size;

    // capacity of min heap
    unsigned capacity;

    // Array of minheap node pointers
    struct MinHeapNode **array;
};

// A utility function allocate a new
// min heap node with given character
// and frequency of the character
struct MinHeapNode *newNode(cF input)
{
    struct MinHeapNode *temp = (struct MinHeapNode *)malloc(
        sizeof(struct MinHeapNode));

    temp->left = temp->right = NULL;
    temp->data.ch = input.ch;
    temp->data.freq = input.freq;

    return temp;
}

// A utility function to create
// a min heap of given capacity
struct MinHeap *createMinHeap(unsigned capacity)

{

    struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

    // current size is 0
    minHeap->size = 0;

    minHeap->capacity = capacity;

    minHeap->array = (struct MinHeapNode **)malloc(
        minHeap->capacity * sizeof(struct MinHeapNode *));
    return minHeap;
}

// A utility function to
// swap two min heap nodes
void swapMinHeapNode(struct MinHeapNode **a,
                     struct MinHeapNode **b)

{

    struct MinHeapNode *t = *a;
    *a = *b;
    *b = t;
}

// The standard minHeapify function.
void minHeapify(struct MinHeap *minHeap, int idx)

{

    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < minHeap->size && minHeap->array[left]->data.freq < minHeap->array[smallest]->data.freq)
        smallest = left;

    if (right < minHeap->size && minHeap->array[right]->data.freq < minHeap->array[smallest]->data.freq)
        smallest = right;

    if (smallest != idx)
    {
        swapMinHeapNode(&minHeap->array[smallest],
                        &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}

// A utility function to check
// if size of heap is 1 or not
int isSizeOne(struct MinHeap *minHeap)
{

    return (minHeap->size == 1);
}

// A standard function to extract
// minimum value node from heap
struct MinHeapNode *extractMin(struct MinHeap *minHeap)

{

    struct MinHeapNode *temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];

    --minHeap->size;
    minHeapify(minHeap, 0);

    return temp;
}

// A utility function to insert
// a new node to Min Heap
void insertMinHeap(struct MinHeap *minHeap,
                   struct MinHeapNode *minHeapNode)

{

    ++minHeap->size;
    int i = minHeap->size - 1;

    while (i && minHeapNode->data.freq < minHeap->array[(i - 1) / 2]->data.freq)
    {

        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }

    minHeap->array[i] = minHeapNode;
}

// A standard function to build min heap
void buildMinHeap(struct MinHeap *minHeap)

{

    int n = minHeap->size - 1;
    int i;

    for (i = (n - 1) / 2; i >= 0; --i)
        minHeapify(minHeap, i);
}

// A utility function to print an array of size n
void printArr(int arr[], int n, char one_res[50])
{
    int i;
    char itoc;
    strcpy(one_res, "");
    for (i = 0; i <= n; ++i){
        
            itoc = arr[i]+'0';

            //If end of string
            if(i >= n){
                one_res[i] = '\0';
                break;
            }
            if(itoc == '0' || itoc == '1'){
                one_res[i] = itoc;
            }
            
    }

    //printf("|| %s\n", one_res);
    //    printf("%d", arr[i]);
}

// Utility function to check if this node is leaf
int isLeaf(struct MinHeapNode *root)

{

    return !(root->left) && !(root->right);
}

// Creates a min heap of capacity
// equal to size and inserts all character of
// data[] in min heap. Initially size of
// min heap is equal to capacity
struct MinHeap *createAndBuildMinHeap(cF input[], int size)

{

    struct MinHeap *minHeap = createMinHeap(size);

    for (int i = 0; i < size; ++i)
        minHeap->array[i] = newNode(input[i]);

    minHeap->size = size;
    buildMinHeap(minHeap);

    return minHeap;
}

// The main function that builds Huffman tree
struct MinHeapNode *buildHuffmanTree(cF input[], int size)

{
    struct MinHeapNode *left, *right, *top;

    // Step 1: Create a min heap of capacity
    // equal to size.  Initially, there are
    // modes equal to size.
    struct MinHeap *minHeap = createAndBuildMinHeap(input, size);

    // Iterate while size of heap doesn't become 1
    while (!isSizeOne(minHeap))
    {

        // Step 2: Extract the two minimum
        // freq items from min heap
        left = extractMin(minHeap);
        right = extractMin(minHeap);

        // Step 3:  Create a new internal
        // node with frequency equal to the
        // sum of the two nodes frequencies.
        // Make the two extracted node as
        // left and right children of this new node.
        // Add this node to the min heap
        // '$' is a special value for internal nodes, not
        // used
        cF input = {'$', left->data.freq + right->data.freq};
        top = newNode(input);

        top->left = left;
        top->right = right;

        insertMinHeap(minHeap, top);
    }

    // Step 4: The remaining node is the
    // root node and the tree is complete.
    return extractMin(minHeap);
}

// Prints huffman codes from the root of Huffman Tree.
// It uses arr[] to store codes
void printCodes(struct MinHeapNode *root, int arr[],
                int top, char res [][50])

{

    // Assign 0 to left edge and recur
    if (root->left)
    {

        arr[top] = 0;
        printCodes(root->left, arr, top + 1, res);
    }

    // Assign 1 to right edge and recur
    if (root->right)
    {

        arr[top] = 1;
        printCodes(root->right, arr, top + 1, res);
    }

    // If this is a leaf node, then
    // it contains one of the input
    // characters, print the character
    // and its code from arr[]
    if (isLeaf(root))
    {

        //printf("%c: ", root->data.ch);
        printArr(arr, top, res[root->data.ch-65]);
    }
}

// The main function that builds a
// Huffman Tree and print codes by traversing
// the built Huffman Tree
void HuffmanCodes(cF input[], int size, char res [][50])

{
    // Construct Huffman Tree
    struct MinHeapNode *root = buildHuffmanTree(input, size);

    // Print Huffman codes using
    // the Huffman tree built above
    int arr[MAX_TREE_HT], top = 0;

    printCodes(root, arr, top, res);
}

int main()
{
    int fd1[2];
    int fd2[2];

    pid_t parent;

    pipe(fd1);
    pipe(fd2);

    parent = fork();

    if (parent > 0)
    {
        FILE *ori = fopen("file.txt", "r");
        cF freq[CHAR_AM];
        // Stores file length (only alphabets)
        int fileLength = 0;
        // Init
        for (int x = 0; x < CHAR_AM; x++)
        {
            freq[x].ch = (char) 65 + x;
            freq[x].freq = 0;
        }
        char ch;
        // Reads character 1 by 1
        while ((ch = fgetc(ori)) != EOF)
        {
            
            // If lowercase
            if ((ch >= 'a') && (ch <= 'z'))
            {
                fileLength++;
                ch = toupper(ch);
                freq[ch - 'A'].ch = ch;
                freq[ch - 'A'].freq += 1;
            }
            // else if uppercase
            else if ((ch >= 'A') && (ch <= 'Z'))
            {
                fileLength++;
                freq[ch - 'A'].ch = ch;
                freq[ch - 'A'].freq += 1;
            }
            else
            {
                continue;
            }
            // else (if neither)
        }
        
        fclose(ori);
        // Sends result to child
        close(fd1[0]);

        write(fd1[1], freq, sizeof(cF) * CHAR_AM);
        // write(fd1[1], &fileLength, sizeof(int));
        close(fd1[1]);

        //Receives child result
        char codes[MAX_TREE_HT+1][50];
        close(fd2[1]);
        read(fd2[0], codes, sizeof(codes));
        close(fd2[0]);
        //Prints results
        for(int x = 0; x < CHAR_AM; x++){
            printf("%c : ", (x+65));
            for(int y = 0; y < 50; y++){
                if(!codes[x][y]){
                    break;
                }
                printf("%c", codes[x][y]);
            }
            printf("\n");
        }
        printf("Initial size (only alphabets) : %d bits\n", fileLength*8);
        //Calculates encrypted size
        FILE *encrypted = fopen("encrypted.txt", "a");
        int fSize;
        // Move stream (cursor) to end of file, measure size of file by position of stream
        fseek(encrypted, 0L, SEEK_END);
        fSize = ftell(ori);
        printf("Size after encryption (only alphabets) : %d bits\n", fSize);
        fclose(encrypted);
        wait(NULL);


    }
    else
    {
        // Receives frequency from  parent
        cF freqCpy[CHAR_AM];
        //2D array to store pairs of characters and huffman codes
        char codes[MAX_TREE_HT+1][50];
        // int fileLength;

        close(fd1[1]);

        read(fd1[0], freqCpy, sizeof(cF) * CHAR_AM);
        // read(fd1[0], &fileLength, sizeof(int));
        // printf("Length : %d\n", fileLength);

        close(fd1[0]);

        // Gets file content
        FILE *ori = fopen("file.txt", "r");
        // Opens encrypted file
        FILE *encrypted = fopen("encrypted.txt", "w");
        int fSize;
        char *buffer;
        // Move stream (cursor) to end of file, measure size of file by position of stream
        fseek(ori, 0L, SEEK_END);
        fSize = ftell(ori);
        // Rewind stream back to beginning
        rewind(ori);
        /*Alocate memory*/
        buffer = calloc(1, fSize + 1);
        // Copy
        fread(buffer, fSize, 1, ori);

        // for(int x = 0; x < 26; x++){
        //     printf("%c || %d\n", freqCpy[x].ch, freqCpy[x].freq);
        // }
        char ch;
        HuffmanCodes(freqCpy, 26, codes);

        //ENCRYPT
        for(int x = 0; x <= fSize; x++){
            ch = *(buffer+x);
            // If lowercase
            if ((ch >= 'a') && (ch <= 'z'))
            {

                ch = toupper(ch);
                fprintf(encrypted, "%s", codes[ch-65]);
            }
            // else if uppercase
            else if ((ch >= 'A') && (ch <= 'Z'))
            {

                fprintf(encrypted, "%s", codes[ch-65]);
            }
            else
            {
                continue;
            }
        }
        //Send huffmancode to parent
        close(fd2[0]);
        write(fd2[1], codes, sizeof(codes));
        close(fd2[1]);
        // Cleanup
        fclose(ori);
        fclose(encrypted);
        free(buffer);
    }
}
