
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>


#define B1 4
#define K2 5


int multiply(int ang, int arr[], int num)
{
	int ans = 0;
	//melakukan perkalian setiap elemen array arr[] dengan ang (pakai bantuan hasil dan ans)
	for(int j=0; j<num; j++) { 	 
    	int hasil;
    	hasil = (arr[j]*ang) + ans;
    	//menyimpan digit terakhir dari produk ke array arr[]
    	arr[j] = hasil%10;       	 
    	ans = hasil/10;    
	}
   
   //menginputkan ans ke array arr[] lalu menambah size array arr[]
  	 
	while(ans) {                 	 
    	arr[num] = ans%10;
    	ans/=10;
    	num++;
	}
	return num;
}


void fakt(int n)  	//fungsi faktorial untuk menghitung nilai faktorial dan print hasilnya
{
	//untuk menyimpan hasil output digit
	int arr[100];     	 


	arr[0] = 1;
	int ans = 1;


	//melakukan faktorial
	for(int num = 2; num <= n; num++){
    	ans = multiply(num, arr, ans);
	}


	//print hasil faktorial
	for(int i = ans - 1; i >= 0; i--){
    	printf("%d", arr[i]);
	}
}


int main()
{
	//buat menghitung execution time
   clock_t start, finish;
	double waktu;
	start = clock();


	int matriks[B1][K2];
	key_t key = 1236;
	int shmid = shmget(key, sizeof(int)*B1*K2, IPC_CREAT | 0666);
	int *shmaddr = (int *)shmat(shmid, NULL, 0);


	printf("hasil kali kedua matriks dari shared memory: \n");
	int ans = 0;
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	printf("%d\t", *(shmaddr + ans));
        	//memasukan ke matriks buat dipake itung faktorial
        	matriks[i][j]=*(shmaddr + ans);
        	ans++;
    	}
    	printf("\n");
	}


	//faktorial tanpa multithread
	printf("\nBerikut ini adalah hasil faktorial (NO-THREAD) perkalian matriks sebelumnya: \n");
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	fakt(matriks[i][j]);
        	printf(" ");
    	}
	}
	printf("\n");


	shmdt((void *)shmaddr);


	//perhitungan waktu eksekusi telah selesai
 	finish = clock();
	waktu = ((double) (finish - start)) / CLOCKS_PER_SEC;
	printf("\n\nWaktu eksekusi (execution time): %f detik\n", waktu);
   
	return 0;
}
