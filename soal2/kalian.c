#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>


// untuk baris dan kolom matriks 1
#define B1 4
#define K1 2
//  untuk baris dan kolom matriks 2
#define B2 2
#define K2 5


int** Matrix(int baris, int kolom, int BatasAtas, int BatasBawah) {
	int** matriks = malloc(baris * sizeof(int*));
	srand(time(NULL));
	for (int n=0; n<baris; n++) {
    	matriks[n] = malloc(kolom * sizeof(int));
    	for (int m=0; m<kolom; m++) {
        	matriks[n][m] = (rand() % (BatasAtas - BatasBawah + 1)) + BatasBawah;
    	}
	}
	return matriks;
}


int main()
{
	int **mat1 = Matrix(B1, K1, 5, 1);
	int **mat2 = Matrix(B2, K2, 4, 1);


	printf("Berikut Matriks Pertama : \n");
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K1; j++){
        	printf("%d\t", mat1[i][j]);
    	}
    	printf("\n");
	}


	printf("\nBerikut Matriks Kedua : \n");
	for(int i=0; i<B2; i++){
    	for(int j=0; j<K2; j++){
        	printf("%d\t", mat2[i][j]);
    	}
    	printf("\n");
	}




	printf("\nHasil Perkalian Dua Matriks :  \n");
	int matriks[B1][K2];
	for(int i=0; i<B1; i++){     	//kurang dari baris matriks a
    	for(int j=0; j<K2; j++){ 	//kurang dari kolom matriks b
        	matriks[i][j] = 0;
        	for(int k=0; k<B2; k++){ //kurang dari baris dari matriks b
            	matriks[i][j] += mat1[i][k]*mat2[k][j];
        	}
    	}
	}


	//shared memory
	key_t key = 1236;   //key shared memory yang akan dipakai
	int shmid = shmget(key, sizeof(int)*B1*K2, IPC_CREAT | 0666);   //id sharedmemory yg akan dipakai
	int *shmaddr = (int *)shmat(shmid, NULL, 0);	//variabel buat simpan isi perkalian matriks, sama aja kyk array satu dimensi


	// copy hasil operasi matriks ke shared memory
	int count = 0;
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	*(shmaddr + count) = matriks[i][j];
        	count++;
    	}
	}


	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	printf("%d\t", matriks[i][j]);
    	}
    	printf("\n");
	}
	//memberi waktu untuk mengrun program cinta dan sisop
	sleep(20);
   
	//detach lalu destroy shared memory
	shmdt((void *)shmaddr);
	shmctl(shmid, IPC_RMID, NULL);
	return 0;
}
