#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>


#define B1 4
#define K2 5


int multiply(int ang, int arr[], int num)
{
	int ans = 0;
	//melakukan perkalian setiap elemen array arr[] dengan ang (pakai bantuan hasil dan ans)
	for(int j=0; j<num; j++) { 	 
    	int hasil;
    	hasil = (arr[j]*ang) + ans;
    	//menyimpan digit terakhir dari hasil ke array arr[]
    	arr[j] = hasil%10;       	 
    	ans = hasil/10;    
	}
   
   //menginputkan ans ke array arr[] lalu menambah size array arr[]
  	 
	while(ans) {                 	 
    	arr[num] = ans%10;
    	ans/=10;
    	num++;
	}
	return num;
}


void *faktorial(void *args)  	//fungsi faktorial untuk menghitung nilai faktorial dan print hasilnya
{
	int arr[100];       	//maksimal output 100 digit (meskipun worstcase 48 digit)
   
	//ambil value dari argumen
	int n;
	n = *((int *)args);


	arr[0] = 1;
	int ans = 1;


	//melakukan faktorial
	for(int i = 2; i <= n; i++){
   	ans = multiply(i, arr, ans);
	}


	//print hasil faktorial
	for(int i = ans - 1; i >= 0; i--){
    	printf("%d", arr[i]);
	}
	return NULL;
}


int main()
{
	//buat menghitung execution time
	clock_t start, finish;
	double waktu;
	start = clock();


	int matriks[B1][K2];
	//untuk mengatur pengaksesan shared memory
	key_t key = 1236;
	int shmid = shmget(key, sizeof(int)*B1*K2, IPC_CREAT | 0666);
	int *shmaddr = (int *)shmat(shmid, NULL, 0);


	printf("hasil kali kedua matriks dari shared memory: \n");
	int ans = 0;
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	printf("%d\t", *(shmaddr + ans));
        	//memasukan ke matriks buat dipake itung faktorial
        	matriks[i][j]=*(shmaddr + ans);
        	ans++;
    	}
    	printf("\n");
	}


	//multithreading faktorial
	printf("\nBerikut ini adalah hasil faktorial (THREAD) perkalian matriks sebelumnya: \n");
	pthread_t tid[B1][K2];
	for(int i=0; i<B1; i++){
    	for(int j=0; j<K2; j++){
        	pthread_create(&tid[i][j], NULL, faktorial, &matriks[i][j]);
        	//melakukan join di setiap thread nya
        	pthread_join(tid[i][j], NULL);
        	printf(" ");
    	}
	}
	printf("\n");


	shmdt((void *)shmaddr);


	//perhitungan waktu eksekusi telah selesai
	finish = clock();
	waktu = ((double) (finish - start)) / CLOCKS_PER_SEC;
	printf("\n\nWaktu eksekusi (execution time): %f detik\n", waktu);


	return 0;
}
