#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#define MAX_LEN 256
#define MAX_FOL 100

//Struct to store amount of files in each folder
struct cell{
    char name[10];
    int amount;
};

struct cell folAmount[MAX_FOL];
struct cell extAmount[MAX_FOL];

int getIndex(char * k, struct cell * arr){
    for(int x = 0; x < 100; x++){
        if(strcmp(k, (arr+x)->name) == 0){
            return x;
        }
    }
    return -1;
}

//Function to sort ascendingly
void swap(struct cell* xp, struct cell* yp)
{
    struct cell temp = *xp;
    *xp = *yp;
    *yp = temp;
}

// Function to perform Selection Sort
void selectionSort(struct cell arr[], int n)
{
    int i, j, min_idx;

    // One by one move boundary of unsorted subarray
    for (i = 0; i < n - 1; i++) {

        // Find the minimum element in unsorted array
        min_idx = i;
        for (j = i + 1; j < n; j++){
            
            if (strcmp(arr[j].name, arr[min_idx].name) < 0){
                min_idx = j;    
            }
        }

        // Swap the found minimum element
        // with the first element
        if(min_idx != i)
            swap(&arr[min_idx], &arr[i]);
    }
}

int main(void)
{
    //Variables that stores data
    int acc = 0;
    int countExt = 0;
    int countFold = 0;


    FILE* fp;
    fp = fopen("log.txt", "r");
    if (fp == NULL) {
      perror("Failed: ");
      return 1;
    }

    char fileName[MAX_LEN]; //Filename
    char buffer[MAX_LEN];
    char buffer2[MAX_LEN]; //Used to get dest file path
    while (fgets(buffer, MAX_LEN, fp))
    {
        strcpy(buffer2, buffer);
        // Remove trailing newline
        buffer[strcspn(buffer, "\n\r")] = 0;
        //Splits string
        char * token = strtok(buffer, " ");
        token = strtok(NULL, " ");
        token = strtok(NULL, " "); //Log type
        if(strcmp(token, "ACCESSED") == 0){
            acc++;
        }
        else if(strcmp(token, "MADE") == 0){
            //Adds folder to list (ascending)
            //Reads created folder path
            token = strtok(NULL, " ");
            char * num = strtok(NULL, " ");
            strcpy(fileName, token);
            if(num && strlen(num) > 0){
                
                strcat(fileName, " ");
                strcat(fileName, num);
            }
            
            token = strtok(fileName, "/");
            char * token2 = strtok(NULL, "/");
            if(token2){
                strcpy(fileName, token2);
            }
            strcpy(folAmount[countFold].name, fileName);
            
            //printf("CREATED : %s\n", folAmount[countFold].name);
            countFold++;
        }
        else if(strcmp(token, "MOVED") == 0){
            //Process file extention
            token = strtok(NULL, " "); //File type
            int ext_id, fold_id;
            ext_id = getIndex(token, extAmount);
            if((countExt < MAX_FOL) && (ext_id < 0)){
                //printf("New extention : %s\n", token);
                strcpy(extAmount[countExt].name, token);
                ext_id = countExt;
                countExt++;
                //printf("MOVED %s\n", token);
                strcpy(fileName, token);
            }
            extAmount[ext_id].amount++;

            //Process folder (destination)
            token = strtok(buffer2, ">");
            token = strtok(NULL, ">");
            char * destFol = strtok(token, "/"); 
            destFol = strtok(NULL, "/"); 
            //strcpy(fileName, destFol);
            fold_id = getIndex(destFol, folAmount);
            folAmount[fold_id].amount++;

            //Add to categorized
            folAmount[getIndex("categorized", folAmount)].amount++;
        }

    }

    printf("ACCESSED : %d\n", acc);
    printf("--------------------------------------------\n");
    selectionSort(folAmount, MAX_FOL);
    selectionSort(extAmount, MAX_FOL);
    //Test for stored folders

    printf("SORTED FOLDER + AMOUNT\n");
    for(int x = 0; x < MAX_FOL; x++){
        if(strcmp(folAmount[x].name, "")==0)
            continue;

        printf("%s : %d\n", folAmount[x].name, folAmount[x].amount);
    }
    printf("--------------------------------------------\n");
    // //Test for stored folders
    printf("SORTED EXT + AMOUNT\n");
    for(int x = 0; x < MAX_FOL; x++){
        if(strcmp(extAmount[x].name, "")==0)
            continue;
        printf("%s : %d\n", extAmount[x].name, extAmount[x].amount);
    }

    fclose(fp);
    return 0;
}