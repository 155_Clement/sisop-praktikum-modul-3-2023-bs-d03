#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//For error codes
#include <errno.h>
#include <sys/stat.h>
//For wait
#include <sys/wait.h>

#include <curl/curl.h>

//Unzip function
int unzipDir(char name[]){
    //Calls exec in child process
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (pid == 0) {
        printf("Unzipping %s (%d)\n", name, getpid());
        char *argv[] = {"zip", "-q", name, NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    else{
        int status;
        do {
            status = wait(NULL);
            if(status == -1 && errno != ECHILD) {
                perror("Error during wait()");
                abort();
            }
        } while (status > 0);
    }

    return(0);
}

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
  size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
  return written;
}

int main(int argc, char *argv[])
{
    CURL *curl_handle;
    static const char *pagefilename = "hehe.zip";
    FILE *pagefile;
    

    
    curl_global_init(CURL_GLOBAL_ALL);
    
    /* init the curl session */
    curl_handle = curl_easy_init();
    
    /* set URL to get here */
    curl_easy_setopt(curl_handle, CURLOPT_URL, "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp");
    
    /* Switch on full protocol/debug output while testing */
    curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);
    
    /* disable progress meter, set to 0L to enable it */
    curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);

      /* Enables redirects */
    curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1L);

      /* Change user agent to firefox, avoids "error 1020" due to curl being blocked*/
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/111.0");

    /* send all data to this function  */
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);
    
    /* open the file */
    pagefile = fopen(pagefilename, "wb");
    if(pagefile) {
    
        /* write the page body to this file handle */
        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, pagefile);
    
        /* get it! */
        curl_easy_perform(curl_handle);
    
        /* close the header file */
        fclose(pagefile);
    }
    
    /* cleanup curl stuff */
    curl_easy_cleanup(curl_handle);
    
    curl_global_cleanup();

    /*-------------------------------------------------*/

    unzipDir("./hehe.zip");
    
    return 0;
}