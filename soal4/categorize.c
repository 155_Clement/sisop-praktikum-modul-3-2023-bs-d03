#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
//For error codes
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
//For threads
#include <pthread.h>
//To access folder contents
#include <dirent.h>
//For nanosleep & time for log
#include <time.h>

#define MAX_LEN 50
#define CATNAME "categorized"
//Amount of storage folders (amount of extentions + "other")
#define folders 8
//Location of log
#define log_path "./log.txt"

//Debug test
#define NORMAL_COLOR  "\x1B[0m"
#define GREEN  "\x1B[32m"
#define BLUE  "\x1B[34m"
#define RED "\x1B[31m"
#define YELLOW "\x1B[33m"
#define MAGENTA "\x1B[36m"

//Initializes MutEx
pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex3 = PTHREAD_MUTEX_INITIALIZER;

//Init values for sleep

// struct timespec {
//     time_t tv_sec;        /* seconds */
//     long   tv_nsec;       /* nanoseconds */
// };

struct timespec tenth, test = {0, 999};


//Struct to store amount of files in each folder
struct folderFiles{
    char name[10];
    int amount;
    int foldAmount;
};

//Global structs so every thread can access it
struct folderFiles fileAmount[folders];
int max; //Max files per folder

//Struct to dynamically store and add t_id for each folder
typedef struct tidLinked{
    pthread_t t_id;
    struct tidLinked *next;
}tidL;

void init(tidL *list){
    list->next = NULL;
    list->t_id;
}

pthread_t * add(tidL *cur){
    tidL *new_node = (tidL*)calloc(1, sizeof(tidL));

    cur->next = new_node;
    new_node->next = NULL;
    return &(new_node->t_id);
}

void clean_up(tidL *list){
    tidL *temp = NULL;

    while(list != NULL){
        temp = list;
        list = list->next;
        pthread_join(temp->t_id, NULL);
        //free(temp);
    }
}
//Function to convert string to lowercase
void to_lower_case(char * in){
    for(int i = 0; in[i]; i++){
        in[i] = tolower(in[i]);
    }
}
//Function to write log

void logging(int type, char arg1[], char arg2[], char arg3[]){
    pthread_mutex_lock(&mutex1);
    FILE * log = fopen(log_path, "a");

    char log_msg[500];
    char time_str[20];
    //Get time
    time_t rawtime;   
    time ( &rawtime );
    struct tm *timeinfo = localtime ( &rawtime );
    strftime(time_str, 20, "%d-%m-%Y %T", timeinfo);

    if(type == 1){
        //Accessed
        fprintf(log, "%s ACCESSED %s\n", time_str, (arg1 + 2));
    }
    else if(type == 2){
        //Moved
        fprintf(log, "%s MOVED %s file : %.*s > %.*s\n", time_str, arg1, (int) strcspn((arg2+3), "\'"),(arg2 + 3), (int) strcspn((arg3 + 3), "\""), (arg3 + 3));
    }
    else if(type == 3){
        //Made
        fprintf(log, "%s MADE %s\n", time_str, arg1);
    }
    
    pthread_mutex_unlock(&mutex1);
    fclose(log);
}

//Function to create new dir in running dir
/*
Args :
- 
*/
void createDir(char name[]){
    int check = mkdir(name, 0777);

    if (!check){
        printf("Directory %s created\n", name);
        logging(3, name, NULL, NULL);
    }
    else{
        printf("ERROR : \"%s\"", name);
        exit(1);
    }
}
//Function to sort ascendingly
void swap(struct folderFiles* xp, struct folderFiles* yp)
{
    struct folderFiles temp = *xp;
    *xp = *yp;
    *yp = temp;
}

// Function to perform Selection Sort
void selectionSort(struct folderFiles arr[], int n)
{
    int i, j, min_idx;

    // One by one move boundary of unsorted subarray
    for (i = 0; i < n - 1; i++) {

        // Find the minimum element in unsorted array
        min_idx = i;
        for (j = i + 1; j < n; j++){
            if (strcmp(arr[j].name, arr[min_idx].name) < 0){
                min_idx = j;    
            }
        }

        // Swap the found minimum element
        // with the first element
        swap(&arr[min_idx], &arr[i]);
    }
}


//Main function
void *recursive(void *args) {

    char dirPath[200];//Folder relative path
    strcpy(dirPath, (char*)args);

    

    printf("%sSTART > Thread managing \"%s\"\n", YELLOW, dirPath);

    //Initializes linked list to store all subthread's id
    tidL tid_list;
    init(&tid_list);

    DIR * d = opendir(dirPath); // open the path
    if(d==NULL) {
        printf("%sERROR : Unable to open %s\n", RED, dirPath);
        return NULL;
        } // if was not able, return
    struct dirent * dir; // for the directory entries
    char f_name[200];//2 uses
    char next_path[200]; // here I am using sprintf which is safer than strcat
    char command[200];
    while ((dir = readdir(d)) != NULL) // if we were able to read somehting from the directory
    {
        

        if(dir-> d_type != DT_DIR){ // if the type is not directory just print it with blue color

            
            
            sprintf(f_name, "%s", dir->d_name);

            //Retrieves extention (.{extention})
            char * ext; //The extention
            char * token = strtok(f_name, ".");
            ext = token;
            // loop through the string to extract all other tokens
            while( token != NULL ) {
                ext = token;
                token = strtok(NULL, ".");
            }
            char d_path[200]; // here I am using sprintf which is safer than strcat
            char dest_path[200];
            sprintf(d_path, "\'%s/%s\'", dirPath, dir->d_name);
            

            //Moves file depending on it's extension
            int other_ext = 0;
            int x = 0;
            int oth = 0;

            //Compares file with every extention in extensions.txt
            char ext2[50];
            sprintf(ext2, "%s", ext);
            to_lower_case(ext2);
            while(x < folders){
                if(!oth && (strcmp(fileAmount[x].name, "other") == 0)){
                    oth = x;
                    continue;
                }
                if(strcmp(fileAmount[x].name, ext2) == 0 ){
                    other_ext = 1;
                    break;
                }
                x++;
            }
            //Moves file
            if (other_ext)
            {
                //LOCK
                
                //If current folder is full
                if(fileAmount[x].amount > 0 && (fileAmount[x].amount % max) == 0){
                    fileAmount[x].foldAmount += 1;
                    sprintf(dest_path, "%s/%s (%d)", CATNAME, fileAmount[x].name, fileAmount[x].foldAmount);
                    createDir(dest_path);
                }
                //Generates destination path (based on amount of folder)
                if(fileAmount[x].foldAmount > 1 && fileAmount[x].amount > 9){
                    sprintf(dest_path, "\"./%s/%s (%d)/%s\"", CATNAME, fileAmount[x].name, fileAmount[x].foldAmount,dir->d_name);
                }
                else if(fileAmount[x].amount < 10){
                    sprintf(dest_path, "\"./%s/%s/%s\"", CATNAME, fileAmount[x].name, dir->d_name);
                }
                //char * commnd = malloc(100);
                
                
                 
                printf("%sMOVING : %s -> %s\n",MAGENTA, d_path, dest_path);
                
                sprintf(command, "mv %s %s", d_path, dest_path);

                if(!system(command)){
                    fileAmount[x].amount += 1; 
                    logging(2, ext2, d_path, dest_path);
                }
                //nanosleep(&tenth, &test);

                //free(commnd);
                //UNLOCK
                //
            }
            else{
                //LOCK
                
                sprintf(dest_path, "\'./%s/other/%s\'", CATNAME, dir->d_name);
                
                printf("%sMOVING : %s -> %s\n",MAGENTA, d_path, dest_path);

                //pthread_mutex_lock(&mutex2);
                //char * command = malloc(100);
                
                sprintf(command, "mv %s %s", d_path, dest_path);

                printf("%s\n", command);
                
                if(!system(command)){
                    fileAmount[oth].amount += 1;
                    logging(2, ext2, d_path, dest_path);
                }
                //free(command);
                //UNLOCK
                //nanosleep(&tenth, &test);

                //pthread_mutex_unlock(&mutex2);
            }
        }
        else
            if(dir -> d_type == DT_DIR && strcmp(dir->d_name,".")!=0 && strcmp(dir->d_name,"..")!=0 ) // if it is a directory
            {
                
                printf("%s %s is creating thread for %s\n",GREEN, dirPath, dir->d_name); // print its name in green
                
                pthread_mutex_lock(&mutex3);
                sprintf(next_path, "%s/%s", dirPath, dir->d_name);

                //printf("CREATE THREAD %s : %d\n", dir->d_name, pthread_create(add(&tid_list), NULL, &recursive, (void *)next_path));// recall with the new path
                pthread_create(add(&tid_list), NULL, &recursive, (void *)next_path);
                logging(1, next_path, NULL, NULL);
                //nanosleep(&tenth, &test);
                //nanosleep(&tenth, &test);
                //sleep(1);
                
                pthread_mutex_unlock(&mutex3);
            }
    }
    printf("%sEXITING > Thread managing \"%s\"\n", YELLOW, dirPath);
    //sleep(1);
    clean_up(&tid_list);

    closedir(d); // finally close the directory

    
    return NULL;
}

int main(){
    pthread_mutex_init(&mutex1, NULL);
    pthread_mutex_init(&mutex2, NULL);
    pthread_mutex_init(&mutex3, NULL);

    FILE * extentions = fopen("./extensions.txt", "r");
    FILE * maxFile = fopen("./max.txt", "r");
    //Untuk menyimpan jumlah file dalam setiap folder hasil, digunakan struct array.

    int count = 0; //Menghitung index

    char catFol[200];
    strcpy(catFol, CATNAME);
    //Creates all storage folders
    createDir(catFol);
    createDir(strcat(catFol, "/other"));
    strcpy(fileAmount[count].name, "other");
    fileAmount[count].amount = 0;
    fileAmount[count].foldAmount = 1;
    count++;
    strcpy(catFol, CATNAME);
    char buffer[MAX_LEN];
    while (fgets(buffer, MAX_LEN, extentions))
    {
        // Remove trailing newline
        buffer[strcspn(buffer, "\n\r")] = 0;
        strcpy(fileAmount[count].name, buffer);
        fileAmount[count].amount = 0;
        fileAmount[count].foldAmount = 1;
        count++;

        strcat(catFol, "/");
        strcat(catFol, buffer);
        createDir(catFol);
        strcpy(catFol, CATNAME);
    }


    //Stores max file in folder
    fgets(buffer, MAX_LEN, maxFile);
    max = atoi(buffer);
    //Sorts order of extention names 
    selectionSort(fileAmount, folders);

    //Starts sorting process
    pthread_t t_id;
    char target[] = "./files";
    //pthread_create(&t_id, NULL, &run, (void *)test);
    pthread_create(&t_id, NULL, &recursive, (void *)target);

    //pthread_join(t_id, NULL);
    sleep(2);
    pthread_join(t_id, NULL);

    pthread_mutex_destroy(&mutex1);
    pthread_mutex_destroy(&mutex2);
    pthread_mutex_destroy(&mutex3);

    printf("\n\n");
    for(int k = 0; k < folders; k++){
        printf("%s : %d\n", fileAmount[k].name, fileAmount[k].amount);
    }

    fclose(extentions);
    fclose(maxFile);
    return 0;
}